CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

A module allow to add prefix, suffix and hide the field title of any type of
field while creating & config the field widget of content type.

Replace long text title with prefix content. It will reduce the time of doing
form alter.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

INSTALLATION
------------

Install the Field Widget module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------

* This module requires no configuration.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Vernit Gupta - https://www.drupal.org/u/vernit
